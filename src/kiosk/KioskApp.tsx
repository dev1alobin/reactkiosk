import React from 'react';

import './css/KioskApp.css';
import { TopBar, OrderList, CartList } from './components';
import { foodItems } from './data/foodItems';

class KioskApp extends React.Component<any, any> {
	// constructor(props : any) {
	// 	super(props);
	// 	// this.state = {
	// 	// 	cart:[],
	// 	// 	total:{}
	// 	// };
	// }

	 onItemSelect (item : any){
		console.log("item ",item)
		const {cart} = this.state
		let cartItem = cart.find(c => c.id === item.id)
		console.log("cartItem ", cartItem)
		const cartItemIndex = cart.indexOf(c => c.id === item.id)
		if(cartItem){
			cartItem['quantity'] = cartItem['quantity'] + 1
			cartItem['totalprice'] = cartItem['quantity'] * item.price
			cart[cartItemIndex] = cartItem
			this.setState({
				cart
			})
		}else{
			cartItem = item
			cart.push({...item, totalprice: item.price})	
		}
		this.setState({cart})
	} 
	 updateCart(item : any, operator: any){
		const {cart} = this.state
		let cartItem = cart.find(c => c.id === item.id)
		const cartItemIndex = cart.indexOf(c => c.id === item.id)
		if(cartItem && operator == '+'){
			cartItem['quantity'] = item.quantity + 1
			cartItem['totalprice'] = item.quantity * item.price
			cart[cartItemIndex] = cartItem
		}else if(cartItem && operator == '-' && cartItem['quantity'] > 1) {
			cartItem['quantity'] = cartItem['quantity'] - 1
			cartItem['totalprice'] = item.quantity * item.price
			cart[cartItemIndex] = cartItem
		}
		else{
			cart.splice(cart[cartItemIndex],1)
		}
		this.setState({cart}) 
	} 
	render() {
		return (
			<div className="kiosk-app">
				<section className="top-bar">
					<TopBar />
				</section>	
				<section className="main-header">
					<span className="main-header-leftsidebar">Cart</span>
            		<span className="main-header-rightsidebar">Food Items</span>
				</section>
				<section className="main-container">			
					<section className="left-sidebar">
						<CartList items={this.props.cart}  updateCart={(item:any, operator: any) => this.updateCart(item, operator)} />					
					</section>
					<section className="right-sidebar">
						<OrderList items={foodItems} onItemSelect={(item:any) => this.props.onItemSelect(item)}/>
					</section>
				</section>
			</div>
		);
	}
}

export default KioskApp;