import { onItemSelect } from "./actions/appActions"
import { connect } from 'react-redux'
import KioskApp from './KioskApp'

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => {
    return {
        onItemSelect: (item) => dispatch(onItemSelect(item))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KioskApp);