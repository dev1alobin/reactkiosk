export const onItemSelect = (item) => ({
    type: 'ADD_ITEM',
    payload: { item: {...item, totalprice: item.price} }
})