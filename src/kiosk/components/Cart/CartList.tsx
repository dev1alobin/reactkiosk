import React from 'react';
import { CartListItem } from './CartListItem';

export const CartList = (props : any) => {
    return (
        <div id="cartListBlock">
            <div className="cart-Items">
                {props.items.map((item : any) => <CartListItem item={item} updateCart={props.updateCart} />)}
                {/*{props.cartItems && props.cartItems.map((item:any) => <CartListItem item={item} />)} */}
                <div className="cart-total">
                    <div className="sub-total"><h4>Sub Total</h4></div>
                    <div className="gst"><h4>GST</h4></div>
                    <div className="total"><h4>Total</h4></div>
                    <div className="discount"><h4>Discount</h4></div>
                    <div className="grand-total"><h4>Grand Total</h4></div>
                </div>
            </div>
        </div>
    );
}