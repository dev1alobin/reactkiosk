import React from 'react';

export const CartListItem = (props : any) => {
    const { item } = props;
    return (   
             
            <div className="cart-list">
            <div className="cart-image"><img src={item.icon} /></div>
            <div className="cart-food-type-image">
                            <div>
                                <img src={item.isVeg? "src/public/img/veg.png" : 'src/public/img/non-veg.png'}/>
                                <span className="cart-item-name">{item.name}</span>
                            </div>
                            <div>
                                <div className="cart-quantity">
                                    <div className="quantity">
                                        <button className="plus-btn" type="button" name="button" value="+" onClick={() => props.updateCart(item, '+')}>
                                             +
                                        </button>{item.quantity}
                                        <button className="minus-btn" type="button" value="+" name="button" onClick={() => props.updateCart(item, '-')}>
                                             -
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
            <div>$ {item.totalprice}</div>
            </div> 
            
    );
}