import React from 'react';
import { OrderListItem } from './OrderListItem';
import { FoodItem } from 'kiosk/data/foodItems';

export const OrderList = (props: any) => {
    console.log(props)
    return(
        <div className="items-div" key="id">
            {props.items.map((item: FoodItem) => <OrderListItem item={item} onItemSelect={props.onItemSelect} />)}
        </div>
    );
}