import React from 'react';

export const OrderListItem = (props: any) => {
    const { item } = props;
    return (
        <div className="order-list-item items" id={item.id}>
            <div className="item-image"><img src={item.icon} onClick={ () => props.onItemSelect(item)}/></div>
            <div className="food-type">
                <div className="small"><img src={item.isVeg? "src/public/img/veg.png" : 'src/public/img/non-veg.png'} className="symbol" />&nbsp;</div>
                <div className="item-name">{item.name}</div>
            </div>
            <div className="price">${item.price} {item.quantity}</div>
        </div>
    );
}