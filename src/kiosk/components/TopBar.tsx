import React from 'react';

export const TopBar = () => (
    <div id="topBar"  className="header-container">
        <h1>Sindoor Restaurant</h1>
    </div>
);