export interface FoodItem {
    id: number,
    icon: string;
    isVeg: boolean;
    quantity: number;
    name: string;
    price: number;
}

export const foodItems:FoodItem[] = [
    { id:1, icon: 'src/public/img/naan.jpg', isVeg: true, quantity : 1, name: 'Naan', price: 2.50 },
    { id:2, icon: 'src/public/img/chicken-briyani.jpg', isVeg: false, quantity : 1, name: 'Chicken Briyani', price: 8.00 },
    { id:3, icon: 'src/public/img/rice+curry.png', isVeg: false, quantity : 1, name: 'Rice + 1 Curry', price: 7.50 },
    { id:4, icon: 'src/public/img/naan.jpg', isVeg: true, quantity : 1, name: 'Sambar vada', price: 3.50 },
    { id:5, icon: 'src/public/img/chicken-tikka.jpg', isVeg: false, quantity : 1, name: 'Chicken Tikka', price: 2.50 },
    { id:6, icon: 'src/public/img/cheese-garlic-naan.jpg', isVeg: true, quantity : 1, name: 'Cheese Garlic Naan', price: 3.50 },
    { id:7, icon: 'src/public/img/naan.jpg', isVeg: false, quantity : 1, name: 'Chicken Briyani + 1 Curry', price: 9.00 },
    { id:8, icon: 'src/public/img/naan.jpg', isVeg: false, quantity : 1, name: 'Rice + 2 Curry', price: 9.00 },
    { id:9, icon: 'src/public/img/naan.jpg', isVeg: true, quantity : 1, name: 'Masala Medu Vada', price: 1.20 },
    { id:10, icon: 'src/public/img/Roll.jpg', isVeg: true, quantity : 1, name: 'Roll', price: 2.00 },
    { id:11, icon: 'src/public/img/naan.jpg', isVeg: false, quantity : 1, name: '1 Plain Naan + 1 Curry', price: 7.00 },
    { id:12, icon: 'src/public/img/naan.jpg', isVeg: false, quantity : 1, name: 'Ch Briyani + 1 Curry + 1 Tikka', price:11.50 },
    { id:13, icon: 'src/public/img/naan.jpg', isVeg: false, quantity : 1, name: 'Rice + 2 Curry + 1 Drink', price: 10.00 },
    { id:14, icon: 'src/public/img/papdi-chat.jpg', isVeg: true, quantity : 1, name: 'Papdi Chat', price: 5.00 },
    { id:15, icon: 'src/public/img/naan.jpg', isVeg: true, quantity : 1, name: 'Mango Lassi', price: 3.50 },
    { id:16, icon: 'src/public/img/naan.jpg', isVeg: false, quantity : 1,name: '2 Plain Naan + 1 Curry', price:8.50 },
    { id:17, icon: 'src/public/img/dosa.jpg', isVeg: true, quantity : 1, name: 'Dosa Plain/Potato', price: 8.00 }
]