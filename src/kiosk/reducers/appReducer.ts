const initialState: any = {
    cart: []
}

export const appReducer = (state = initialState, action: any) => {
    switch(action.type) {
        case 'ADD_ITEM':
            return { ...state, cart: [...state.cart, action.payload.item] }
        default:
            return state;
    }
}